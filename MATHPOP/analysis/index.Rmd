---
title: "Home"
site: workflowr::wflow_site
output:
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

Welcome to MATHPOP, a `R` software for inferring globular cluster counts in low-surface brightness galaxies (LSBGs) and ultra-diffuse galaxies (UDGs).

See this [vignette](vignette.html) for a quick tutorial on how to use it.

